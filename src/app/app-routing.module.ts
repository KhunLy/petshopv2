import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { PetComponent } from './components/pet/pet.component';
import { PetDetailsComponent } from './components/pet-details/pet-details.component';
import { RegisterComponent } from './components/register/register.component';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { IsNotLoggedGuard } from './guards/is-not-logged.guard';
import { IsAdminGuard } from './guards/is-admin.guard';
import { PetUpdateComponent } from './components/pet-update/pet-update.component';
import { PetAddComponent } from './components/pet-add/pet-add.component';
import { CartComponent } from './components/cart/cart.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PetResolver } from './resolvers/pet.resolver';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [ IsNotLoggedGuard ] },
  { path: 'register', component: RegisterComponent, canActivate: [ IsNotLoggedGuard ] },
  { path: 'cart', component: CartComponent, canActivate: [ IsLoggedGuard ] },
  { path: 'pet', component: PetComponent },
  { path: 'pet-details/:id', component: PetDetailsComponent, resolve: { model: PetResolver } },
  { path: 'pet-update/:id', component: PetUpdateComponent, canActivate: [ IsLoggedGuard, IsAdminGuard ], resolve: { model: PetResolver } },
  { path: 'pet-add', component: PetAddComponent, canActivate: [ IsLoggedGuard, IsAdminGuard ] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [ IsLoggedGuard, IsAdminGuard ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
