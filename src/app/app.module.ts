import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbInputModule, NbDialogModule, NbDatepickerModule, NbToastrModule, NbIconModule, NbSelectModule, NbCardModule, NbActionsModule, NbCheckboxModule, NbAccordionModule, NbListModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { PetComponent } from './components/pet/pet.component';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { PetAddComponent } from './components/pet-add/pet-add.component';
import { PetUpdateComponent } from './components/pet-update/pet-update.component';
import { PetDetailsComponent } from './components/pet-details/pet-details.component';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MatPaginatorTranslateConfig } from './services/mat-paginator-translate.config';
import { DefaultImagePipe } from './pipes/default-image.pipe';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { CartComponent } from './components/cart/cart.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    PetComponent,
    PetAddComponent,
    PetUpdateComponent,
    PetDetailsComponent,
    ConfirmDialogComponent,
    LoaderComponent,
    DefaultImagePipe,
    CartComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbActionsModule,
    NbButtonModule,
    NbInputModule,
    NbDialogModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbToastrModule.forRoot(),
    NbIconModule,
    NbSelectModule,
    NbCardModule, 
    MatTableModule,
    NbCheckboxModule,
    NbAccordionModule,
    NbListModule,
    MatPaginatorModule,
    ChartsModule
  ],
  providers: [
    { provide: MatPaginatorIntl, useClass: MatPaginatorTranslateConfig },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
