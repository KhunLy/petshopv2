import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbMenuItem, NbSidebarService, NbToastrService } from '@nebular/theme';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  isLogged: boolean

  items: NbMenuItem[]

  constructor(
    private session: SessionService,
    private toastr: NbToastrService,
    private sidebar: NbSidebarService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.items = [];
    this.session.onUser$.subscribe((user) => {
      this.items = [];
      this.isLogged = user != null;
      this.items = this.items.concat([
        { icon: 'home', title: 'Accueil', link: '/home' },
        { icon: 'github', title: 'Animaux de compagnie', link: '/pet' },
      ]);
      if(this.isLogged) {
        this.items = this.items.concat([]);
        if(this.session.user?.roles.includes('ADMIN')) {
          this.items = this.items.concat([
            { icon: 'monitor', title: 'DashBoard', link: '/dashboard' },
          ]);
        }
      }
    });
  }

  toggle() {
    this.sidebar.toggle(true);
  }

  logout() {
    this.session.close();
    this.router.navigateByUrl('/auth').then(() => {
      this.toastr.primary("Good Bye")
    });
  }
}
