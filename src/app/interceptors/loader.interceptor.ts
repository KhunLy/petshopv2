import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from '../components/loader/loader.component';
import { delay, finalize } from 'rxjs/operators'

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private dialogService: NbDialogService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
    if(req.reportProgress) {
      return next.handle(req);
    }
    let ref = this.dialogService.open(LoaderComponent, { closeOnBackdropClick: false });
    return next.handle(req).pipe(delay(500),finalize(() => {
      setTimeout(() => {
        ref.close();
      }, 500);
    }));

  }
}
