import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PetModel } from '../models/pet.model';
import { PetService } from '../services/pet.service';

@Injectable({
  providedIn: 'root'
})
export class PetResolver implements Resolve<PetModel> {

  constructor(
    private petService: PetService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): PetModel | Observable<PetModel> | Promise<PetModel> {
    let id = parseInt(route.params.id);
    return this.petService.getDetails(id);
    //return { id: 1, reference: 'ref1', isVaccinated: true, statusName: 'BOOKED', breedId: 1, breedName: 'Cat', birthDate: new Date('2000-01-01')  }
  }
}
