import { PetModel } from "./pet.model";

export interface PetIndexModel {
    offset: number
    limit: number
    count: number
    results: PetModel[]
}