export interface RegisterModel {
    id: number
    username: string
    password: string
    lastName: string
    firstName: string
}