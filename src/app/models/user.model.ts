export interface UserModel {
    id: number
    username: string
    password: string
    roles: string []
    lastName: string
    firstName: string
    token?: string
}