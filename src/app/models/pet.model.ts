export interface PetModel {
    id: number
    reference: string

    image?: string
    imageFile?: File

    vaccinated: boolean
    birthDate?: Date
    breedId: number
    breedName: string

    userId?: number
    statusName: string
}
