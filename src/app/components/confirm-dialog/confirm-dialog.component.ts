import { Component, Input, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  message: string;

  constructor(
    private ref: NbDialogRef<ConfirmDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  yes() {
    this.ref.close(true);
  }

  no() {
    this.ref.close(false);
  }

}
