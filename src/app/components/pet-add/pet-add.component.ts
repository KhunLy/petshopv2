import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { PetService } from 'src/app/services/pet.service';
import { BreedService } from 'src/app/services/breed.service';

@Component({
  selector: 'app-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.scss']
})
export class PetAddComponent implements OnInit {

  formGroup: FormGroup

  imageSource: any

  constructor(
    private petService: PetService,
    public breedService: BreedService,
    private toastr: NbToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      'reference': new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'imageFile': new FormControl(null),
      'vaccinated': new FormControl(false),
      'birthDate': new FormControl(null),
      'breedId': new FormControl(null)
    });
  }

  submit() {
    this.petService.add(this.formGroup.value).subscribe(response => {
      this.toastr.success('Insertion OK');
      this.router.navigateByUrl('/pet');
    }, xhr => {
      this.toastr.danger('An error occurs')
    });
  }

  fileChangeEvent(event: any): void {

    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = e => {
      this.imageSource = e.target.result;
      this.formGroup.get("imageFile").setValue(this.imageSource);
    };
  }
}
