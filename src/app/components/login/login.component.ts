import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { from } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formGroup:FormGroup

  constructor(
    private auth: AuthService,
    private router: Router,
    private toastrService: NbToastrService,
    private session: SessionService,
  ) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      'username' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
    });
  }


  submit() {
    this.auth.login(this.formGroup.value).subscribe(data => {
      this.session.begin(data);
      this.router.navigateByUrl('/').then(() => this.toastrService.success("Welcome " + this.session.user.firstName));
    }, (xhr) => {
      this.toastrService.danger(xhr.error);
    }, () => {

    });
  }

}
