import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { PetModel } from 'src/app/models/pet.model';
import { BreedService } from 'src/app/services/breed.service';
import { PetService } from 'src/app/services/pet.service';

@Component({
  selector: 'app-pet-update',
  templateUrl: './pet-update.component.html',
  styleUrls: ['./pet-update.component.scss']
})
export class PetUpdateComponent implements OnInit {

  formGroup: FormGroup

  imageSource: any

  model: PetModel

  constructor(
    private petService: PetService,
    public breedService: BreedService,
    private toastr: NbToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.model;
    this.imageSource = this.model.image;
    this.formGroup = new FormGroup({
      'reference': new FormControl(this.model.reference, Validators.compose([
        Validators.required
      ])),
      'imageFile': new FormControl(this.model.image),
      'vaccinated': new FormControl(this.model.vaccinated),
      'birthDate': new FormControl(this.model.birthDate),
      'breedId': new FormControl(this.model.breedId),
      'id' : new FormControl(this.model.id)
    });
  }

  submit() {
    this.petService.edit(this.formGroup.value).subscribe(response => {
      this.toastr.success('Update OK');
      this.router.navigateByUrl('/pet');
    }, xhr => {
      this.toastr.danger('An error occurs')
    });
  }

  fileChangeEvent(event: any): void {
    // this.formGroup.get("imageFile").setValue(event.target.files[0]);
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = e => {
      this.imageSource = e.target.result;
      this.formGroup.get("imageFile").setValue(this.imageSource);
    };
  }
}
