import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { PetModel } from 'src/app/models/pet.model';
import { PetService } from 'src/app/services/pet.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-pet-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.scss']
})
export class PetDetailsComponent implements OnInit {

  model: PetModel

  constructor(
    private petService: PetService,
    public session: SessionService,
    private toastr: NbToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.model;
    console.log(this.model);
  }

  addToCart() {
    this.petService.book(this.model.id, this.session.user?.id).subscribe(() =>  {
      this.toastr.success('Animal ajouté au panier');
      this.router.navigateByUrl('/cart');
    }, error => {
      this.toastr.danger('An error occurs');
    });
  }

}
