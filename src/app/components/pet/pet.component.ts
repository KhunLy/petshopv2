import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { PetIndexModel } from 'src/app/models/pet-index.model';
import { PetModel } from 'src/app/models/pet.model';
import { BreedService } from 'src/app/services/breed.service';
import { PetService } from 'src/app/services/pet.service';
import { SessionService } from 'src/app/services/session.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss']
})
export class PetComponent implements AfterViewInit, OnInit {

  displayedColumns: string[] = ['image', 'reference', 'breedName', 'statusName', 'actions'];

  fg: FormGroup;

  model: PetIndexModel;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private petService: PetService,
    public session: SessionService,
    private toastr: NbToastrService,
    private dialogService: NbDialogService,
    public breedService: BreedService
  ) { }

  ngOnInit() {
    this.fg = new FormGroup({
      'statusName': new FormControl(this.session.user?.roles.includes("ADMIN") ? 'BOOKED' : 'FREE'),
      'breedId': new FormControl(null)
    });
  }

  ngAfterViewInit(): void {
    this.petService.getIndex(0, 5, this.fg.get('statusName').value).subscribe(data => {
      this.model = data;
    });

    this.paginator.page.subscribe(page => {
      this.petService.getIndex(
        (page.pageIndex),
        5,
        this.fg.get('statusName').value,
        this.fg.get('breedId').value
      ).subscribe(data => {
        this.model = data;
      });
    });
  }

  delete(model: PetModel) {
    this.dialogService.open(ConfirmDialogComponent, {
      context: { message: 'Confirmer la suppresion de ' + model.reference }
    }).onClose.subscribe(response => {
      if(response) {
        this.petService.delete(model.id).subscribe(response => {
          this.petService.getIndex(
            this.model.offset,
            this.model.limit,
            this.fg.get('statusName').value,
            this.fg.get('breedId').value
          ).subscribe(data => {
            this.model = data;
          });
          this.toastr.success("Status Updated")
        }, () => {
          this.toastr.danger("An error occurs");
        });
      }
    });
  }

  accept(model: PetModel) {
    this.dialogService.open(ConfirmDialogComponent, {
      context: { message: 'Accepter la demande pour ' + model.reference }
    }).onClose.subscribe(response => {
      if(response) {
        this.petService.accept(model.id, this.session.user?.id).subscribe(response => {
          this.petService.getIndex(
            this.model.offset,
            this.model.limit,
            this.fg.get('statusName').value,
            this.fg.get('breedId').value
          ).subscribe(data => {
            this.model = data;
          });
          this.toastr.success("Item deleted")
        }, () => {
          this.toastr.danger("An error occurs");
        });
      }
    });
  }

  search() {
    this.petService.getIndex(
      0,
      5,
      this.fg.get('statusName').value,
      this.fg.get('breedId').value
    ).subscribe(data => {
      console.log(data);
      this.model = data;
    });
  }
}
