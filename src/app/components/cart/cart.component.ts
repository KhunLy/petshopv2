import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { PetIndexModel } from 'src/app/models/pet-index.model';
import { PetModel } from 'src/app/models/pet.model';
import { PetService } from 'src/app/services/pet.service';
import { SessionService } from 'src/app/services/session.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  constructor(
    private petService: PetService,
    public session: SessionService,
    private toastr: NbToastrService,
    private dialogService: NbDialogService
  ) { }

  model: PetIndexModel

  ngOnInit(): void {
    this.petService.getCart(this.session.user?.id).subscribe(data => {
      this.model = data;
    });
  }
  
  cancel(item:PetModel) {
    this.dialogService.open(ConfirmDialogComponent, { 
      context: { message: 'Confirmer la suppresion de ' + item.reference + ' du votre panier'} 
    }).onClose.subscribe(response => {
      if(response) {
        this.petService.cancel(item.id, this.session.user?.id).subscribe(() => {
          this.toastr.info('Animal retiré du panier');
          this.petService.getCart(this.session.user?.id).subscribe(data => {
            this.model = data;
          });
        }, error => {
          this.toastr.danger('An error occurs')
        })
      }
    });
  }
}
