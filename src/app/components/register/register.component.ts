import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { AuthService } from 'src/app/services/auth.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formGroup:FormGroup

  constructor(
    private auth: AuthService,
    private router: Router,
    private toastrService: NbToastrService,
    private session: SessionService,
  ) { }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
      'username' : new FormControl(null, Validators.compose([
        Validators.minLength(2),
        Validators.required
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'lastName' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'firstName' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
    });
  }


  submit() {
    this.auth.register(this.formGroup.value).subscribe(data => {
      this.router.navigateByUrl('/login').then(() => this.toastrService.success("Registration complete, you can now login"));
    }, (xhr) => {
      this.toastrService.danger(xhr.error);
    }, () => {

    });
  }

}
