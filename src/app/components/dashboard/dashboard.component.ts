import { Component, OnInit } from '@angular/core';
import { Label } from 'ng2-charts';
import { map } from 'rxjs/operators';
import { PetService } from 'src/app/services/pet.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public colors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)', 'rgba(0,255,255,0.3)', 'rgba(255,0,255,0.3)'],
    },
  ];
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  

  public barChartLabels: Label[] = [];
  public barChartData: number[] = [];
  public barChartOption = {
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  }

  start: Date;

  end: Date;

  constructor(
    private petService: PetService
  ) { }

  ngOnInit(): void {
    this.petService.getIndex(0,0).subscribe(data => {
      let temp = {};
      let temp2 = {};
      data.results.forEach(element => {
        if(temp[element.breedName] == null){
          temp[element.breedName] = 0;
        }
        temp[element.breedName]++;

        if(temp2[element.statusName] == null){
          temp2[element.statusName] = 0;
        }
        temp2[element.statusName]++;
      });
      for (let key in temp) {
        this.pieChartLabels.push(key);
        this.pieChartData.push(temp[key])
      }

      for (let key in temp2) {
        this.barChartLabels.push(key);
        this.barChartData.push(temp2[key])
      }
    })
  }


  getRapport() {
    this.petService.rapport(this.start, this.end).subscribe(response => {
      console.log(response);
      let file = new Blob([response as unknown as BlobPart], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}),
        fileName: "rapport animaux";
      var fileURL = URL.createObjectURL(file);
      fileURL.anchor("rapport")
      window.open(fileURL);
      //this.downLoadFile(data, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    });
  }

  downLoadFile(data: any, type: string) {
    let blob = new Blob([data], { type: type});
    let url = window.URL.createObjectURL(blob);
    let pwa = window.open(url);
    if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
        alert( 'Please disable your Pop-up blocker and try again.');
    }
  }

}
