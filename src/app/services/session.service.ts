import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserModel } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public onUser$: BehaviorSubject<UserModel>

  public get user() : UserModel{
    if(localStorage.getItem("USER"))
      return JSON.parse(localStorage.getItem("USER"));
    else
      return null;
  }

  constructor() { 
    if(localStorage.getItem("USER"))
      this.onUser$ = new BehaviorSubject(JSON.parse(localStorage.getItem("USER")));
    else
      this.onUser$ = new BehaviorSubject(null);
  }

  begin(user: UserModel) {
    localStorage.setItem("USER", JSON.stringify(user));
    this.onUser$.next(user);
  }

  close() {
    localStorage.clear();
    this.onUser$.next(null);
  }
}
