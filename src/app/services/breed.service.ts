import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BreedModel } from '../models/breed.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BreedService {

  onBreeds$: BehaviorSubject<BreedModel[]>

  public get breeds(): BreedModel[] {
    return this.onBreeds$.value;
    //return [{ id: 1, name: 'cat' }, { id: 2, name: 'dog' }, { id: 3, name: 'horse' }]
  }

  constructor(
    private http: HttpClient
  ) {
    this.onBreeds$ = new BehaviorSubject<BreedModel[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<BreedModel[]>(environment.api + '/breed').subscribe((data) => {
      this.onBreeds$.next(data);
    });
  }
}
