import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from '../models/login.model';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(model: LoginModel) : Observable<UserModel> {
    return this.http.post<UserModel>(environment.api + '/auth/login', model);
  }

  register(model: UserModel) : Observable<boolean> {
    return this.http.post<boolean>(environment.api + '/auth/register', model);
  }
}
