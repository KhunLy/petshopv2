import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { PetIndexModel } from '../models/pet-index.model';
import { PetModel } from '../models/pet.model';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  constructor(
    private http: HttpClient,
  ) { }

  getIndex(offset: number = 0, limit: number = 20, statusName :string = null, breedId: number = null)
    :Observable<PetIndexModel> {
    let params = new HttpParams();
    let request = "/pet?offset=" + offset + "&limit=" + limit;
    params.set('offset', offset.toString());
    params.set('limit', limit.toString());
    if(breedId != null)
      request += "&breedId=" + breedId;
    if(statusName != null)
      request += "&statusName=" + statusName;
    return this.http.get<PetIndexModel>(environment.api + request);
    // return new Observable(x => {
    //   setTimeout(() => {
    //     x.next(<PetIndexModel>{ limit: 1, count: 3, results:[
    //       { id: 1, reference: 'ref test 1' },
    //       { id: 2, reference: 'ref test 2' },
    //       { id: 3, reference: 'ref test 3' },
    //     ] });
    //   }, 1000)
    // })
  }

  getCart(userId: number) {
    return this.http.get<PetIndexModel>(environment.api + '/pet/cart/' + userId);
  }

  getDetails(id: number) : Observable<PetModel> {
    return this.http.get<PetModel>(environment.api + '/pet/' + id);
  }

  add(model: PetModel) : Observable<boolean> {
    return this.http.post<boolean>(environment.api + '/pet', model);
  }

  edit(model: PetModel) : Observable<boolean> {
    return this.http.put<boolean>(environment.api + '/pet', model);
  }

  delete(id: number) : Observable<boolean> {
    return this.http.delete<boolean>(environment.api + '/pet/' + id);
  }

  book(petId : number, userId: number) : Observable<boolean> {
    return this.http.patch<boolean>(environment.api + '/pet/' + petId, {userId: userId, statusName: 'BOOKED'});
  }

  cancel(petId : number, userId: number) : Observable<boolean> {
    return this.http.patch<boolean>(environment.api + '/pet/' + petId, {userId: userId, statusName: 'FREE'});
  }

  accept(petId : number, userId: number) : Observable<boolean> {
    return this.http.patch<boolean>(environment.api + '/pet/' + petId, {userId: userId, statusName: 'ACCEPTED'});
  }

  rapport(startDate: Date, endDate: Date ) {
    const httpOptions = {
      'responseType'  : 'arraybuffer' as 'json'
    };
    return this.http.post<string[]>(environment.api + "/rapport/animaux", {
      begin: startDate,
      end: endDate
    }, httpOptions);
  }

  private toFormData(model) {
    let result = new FormData();
    for(let p in model) {
      result.append(p, model[p]);
    }
    return result;
  }
}
